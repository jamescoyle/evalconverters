﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace EvalConverters
{
	public class SelfExtension : MarkupExtension
	{
		public SelfExtension()
		{
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return RelativeSource.Self;
		}
	}
}
