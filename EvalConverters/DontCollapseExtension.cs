﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace EvalConverters
{
	public class DontCollapseExtension : MarkupExtension, IValueConverter, IMultiValueConverter
	{
		public string If { get; set; }

		public DontCollapseExtension()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool result = false;
			if (If != null)
			{
				Dictionary<string, object> dict = new Dictionary<string, object>()
				{
					{ "a", value },
					{ "value", value },
					{ "targetType", targetType },
					{ "parameter", parameter },
					{ "culture", culture }
				};
				result = System.Convert.ToBoolean(Setup.Runner.Run(If, s => dict[s]));
			}
			else
				result = System.Convert.ToBoolean(value);
			return result ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}


		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (If == null)
				throw new NotImplementedException();
			Dictionary<string, object> dict = new Dictionary<string, object>()
			{
				{ "values", values },
				{ "targetType", targetType },
				{ "parameter", parameter },
				{ "culture", culture }
			};
			for (int i = 0; i < values.Length; i++)
				dict[((char)(97 + i)).ToString()] = values[i];
			bool result = System.Convert.ToBoolean(Setup.Runner.Run(If, s => dict[s]));
			return result ? Visibility.Visible : Visibility.Collapsed;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}


		public override object ProvideValue(IServiceProvider provider) => this;
	}
}
