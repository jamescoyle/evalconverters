﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace EvalConverters
{
	public class ForEach : FrameworkElement
	{

		public static readonly DependencyProperty ItemTemplateProperty = 
			DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(ForEach), new PropertyMetadata(null, ItemTemplateChanged));
		private static void ItemTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var forEach = (ForEach)d;
			if (e.OldValue != null && forEach.ItemsParent != null)
				forEach.RemoveChildrenFrom(forEach.ItemsParent);
			if (forEach._items == null)
				forEach.Setup();
		}
		public DataTemplate ItemTemplate
		{
			get => (DataTemplate)GetValue(ItemTemplateProperty);
			set => SetValue(ItemTemplateProperty, value);
		}


		public static readonly DependencyProperty ItemsParentProperty = 
			DependencyProperty.Register("ItemsParent", typeof(Panel), typeof(ForEach), new PropertyMetadata(null, ItemsParentChanged));
		private static void ItemsParentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var forEach = (ForEach)d;
			if (e.OldValue is Panel)
				forEach.RemoveChildrenFrom((Panel)e.OldValue);
			if (forEach._items == null)
				forEach.Setup();
		}
		public Panel ItemsParent
		{
			get => (Panel)GetValue(ItemsParentProperty);
			set => SetValue(ItemsParentProperty, value);
		}
		protected override void OnVisualParentChanged(DependencyObject oldParent)
		{
			if (ItemsParent == null && VisualParent is Panel)
				ItemsParent = (Panel)VisualParent;
		}


		public static readonly DependencyProperty ItemsSourceProperty = 
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(ForEach), new PropertyMetadata(null, ItemsSourceChanged));
		private static void ItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var forEach = (ForEach)d;
			if (e.OldValue is INotifyCollectionChanged)
				((INotifyCollectionChanged)e.OldValue).CollectionChanged -= forEach.OnItemsSourceCollectionChanged;
			if (forEach._items == null)
				forEach.Setup();
			else
				forEach.UpdateFromItems();
			if (e.NewValue is INotifyCollectionChanged)
				((INotifyCollectionChanged)e.NewValue).CollectionChanged += forEach.OnItemsSourceCollectionChanged;
		}
		public IEnumerable ItemsSource
		{
			get => (IEnumerable)GetValue(ItemsSourceProperty);
			set => SetValue(ItemsSourceProperty, value);
		}
		private void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			UpdateFromItems();
		}


		private Dictionary<object, FrameworkElement> _items = null;

		private void Setup()
		{
			if (ItemTemplate == null || ItemsParent == null || ItemsSource == null)
				return;
			
			_items = new Dictionary<object, FrameworkElement>();
			foreach(var item in ItemsSource)
				AddChildItem(item);
		}

		private void RemoveChildrenFrom(Panel panel)
		{
			foreach(var item in _items)
				panel.Children.Remove(item.Value);
			_items = null;
		}

		private void AddChildItem(object item)
		{
			var child = ItemTemplate.LoadContent() as FrameworkElement;

			if (child != null)
			{
				child.DataContext = item;

				ItemsParent.Children.Add(child);
				var enumerator = child.GetLocalValueEnumerator();
				while (enumerator.MoveNext())
				{
					var bind = enumerator.Current;
					if (bind.Value is BindingExpression)
						child.SetBinding(bind.Property, ((BindingExpression)bind.Value).ParentBinding);
				}
				_items[item] = child;
			}
		}

		private void UpdateFromItems()
		{
			var template = ItemTemplate;
			var parent = ItemsParent;
			var items = ItemsSource;
			
			Dictionary<object, FrameworkElement> itemsToProcess = _items.ToDictionary(x => x.Key, x => x.Value);
			foreach(var item in items ?? new object[0])
			{
				if (_items.ContainsKey(item))
					itemsToProcess.Remove(item);
				else
					AddChildItem(item);
			}
			foreach (var item in itemsToProcess)
			{
				ItemsParent.Children.Remove(item.Value);
				_items.Remove(item.Key);
			}
		}
	}
}
