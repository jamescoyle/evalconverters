﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Windows.Data;
using System.Globalization;

namespace EvalConverters
{
	public class ToExtension : MarkupExtension, IValueConverter, IMultiValueConverter
	{
		public string To { get; set; }

		public string Back { get; set; }

		public ToExtension()
		{
		}
		
		public ToExtension(string to)
		{
			To = to;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (To == null)
				throw new NotImplementedException();
			Dictionary<string, object> dict = new Dictionary<string, object>()
			{
				{ "a", value },
				{ "value", value },
				{ "targetType", targetType },
				{ "parameter", parameter },
				{ "culture", culture }
			};
			var result = Setup.Runner.Run(To, s => dict[s]);
			if(result is IConvertible)
				return System.Convert.ChangeType(result, targetType);
			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(Back == null)
				throw new NotImplementedException();
			Dictionary<string, object> dict = new Dictionary<string, object>()
			{
				{ "value", value },
				{ "a", value },
				{ "targetType", targetType },
				{ "parameter", parameter },
				{ "culture", culture }
			};
			var result = Setup.Runner.Run(Back, s => dict[s]);
			if (result is IConvertible)
				return System.Convert.ChangeType(result, targetType);
			return result;
		}


		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (To == null)
				throw new NotImplementedException();
			Dictionary<string, object> dict = new Dictionary<string, object>()
			{
				{ "values", values },
				{ "targetType", targetType },
				{ "parameter", parameter },
				{ "culture", culture }
			};
			for (int i = 0; i < values.Length; i++)
				dict[((char)(97 + i)).ToString()] = values[i];
			var result = Setup.Runner.Run(To, s => dict[s]);
			if (result is IConvertible)
				return System.Convert.ChangeType(result, targetType);
			return result;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			if (Back == null)
				throw new NotImplementedException();
			Dictionary<string, object> dict = new Dictionary<string, object>()
			{
				{ "value", value },
				{ "a", value },
				{ "parameter", parameter },
				{ "culture", culture }
			};
			return ((IEnumerable<object>)Setup.Runner.Run(Back, s => dict[s])).ToArray();
		}


		public override object ProvideValue(IServiceProvider provider) => this;
	}
}
