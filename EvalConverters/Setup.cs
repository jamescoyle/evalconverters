﻿using EvalScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace EvalConverters
{
	public static class Setup
	{
		private static Runner _runner;
		public static Runner Runner
		{
			get
			{
				if (_runner == null)
				{
					_runner = new Runner();
					_runner.Interpreter.StringLiteralChar = '`';
					AddAdditionalFunctions(_runner);
				}
				return _runner;
			}
			set => _runner = value;
		}

		public static void AddAdditionalFunctions(Runner runner)
		{
			runner.Evaluator.Functions["Star"] = Star;
			runner.Evaluator.Functions["Auto"] = Auto;
			runner.Evaluator.Functions["Pixels"] = Pixels;

			runner.Evaluator.Functions["Color"] = Color;
			runner.Evaluator.Functions["Brush"] = Brush;
			runner.Evaluator.Functions["LinearGradient"] = LinearGradient;
			runner.Evaluator.Functions["Start"] = Start;
			runner.Evaluator.Functions["End"] = End;
			runner.Evaluator.Functions["RadialGradient"] = RadialGradient;
			runner.Evaluator.Functions["Origin"] = Origin;
			runner.Evaluator.Functions["Center"] = Center;
			runner.Evaluator.Functions["Radius"] = Radius;
			runner.Evaluator.Functions["GradientStops"] = GradientStops;
		}

		private static object Star(object[] args)
		{
			if (args.Length == 0)
				return new GridLength(1, GridUnitType.Star);
			if (args.Length > 1)
				throw new Exception("Star expects 1 optional parameter: double value");
			return new GridLength(Convert.ToDouble(args[0]), GridUnitType.Star);
		}

		private static object Auto(object[] args)
		{
			if (args.Length > 0)
				throw new Exception("Auto expects no parameters");
			return new GridLength(0, GridUnitType.Auto);
		}

		private static object Pixels(object[] args)
		{
			if(args.Length != 1)
				throw new Exception("Pixels expects 1 parameter: double value");
			return new GridLength(Convert.ToDouble(args[0]));
		}

		private static List<PropertyInfo> _predefinedColors;
		private static object _predefinedColorsLock = new object();
		private static List<PropertyInfo> PredefinedColors
		{
			get
			{
				if (_predefinedColors == null)
				{
					lock(_predefinedColorsLock)
					{
						if(_predefinedColors == null)
							_predefinedColors = typeof(Colors).GetProperties().ToList();
					}
				}
				return _predefinedColors;
			}
		}

		private static object Color(object[] args)
		{
			if (args.Length != 1 || !(args[0] is string))
				throw new Exception("Color expects 1 parameter: string name");
			string code = args[0].ToString();
			if(code.StartsWith("#"))
			{
				code = code.Substring(1);
				List<string> partStrings = Enumerable.Range(0, code.Length / 2).Select(i => code.Substring(i * 2, 2)).ToList();
				List<byte> parts = partStrings.Select(x => byte.Parse(x, System.Globalization.NumberStyles.HexNumber)).ToList();

				if (parts.Count == 3)
					return new Color() { R = parts[0], G = parts[1], B = parts[2] };
				else if (parts.Count == 4)
					return new Color() { A = parts[0], R = parts[1], G = parts[2], B = parts[3] };
				else
					throw new Exception($"Invalid color code {code}");
			}
			else
				return (Color)PredefinedColors.First(x => x.Name == code).GetValue(null);
		}

		private static object Brush(object[] args)
		{
			if (args.Length != 1 || (!(args[0] is string) && !(args[0] is Color)))
				throw new Exception("Brush expects 1 parameter: string/Color color");
			if (args[0] is Color)
				return new SolidColorBrush((Color)args[0]);
			return new SolidColorBrush((Color)Color(args));
		}

		private static object LinearGradient(object[] args)
		{
			if (args.Length > 0)
				throw new Exception("LinearGradient expects no parameters");
			var linearGradient = new LinearGradientBrush();
			return linearGradient;
		}

		private static object Start(object[] args)
		{
			if (args.Length != 3 || !(args[0] is LinearGradientBrush))
				throw new Exception("Start expects 3 parameters: LinearGradientBrush brush, double x, double y");
			var brush = (LinearGradientBrush)args[0];
			brush.StartPoint = new Point(Convert.ToDouble(args[1]), Convert.ToDouble(args[2]));
			return brush;
		}

		private static object End(object[] args)
		{
			if (args.Length != 3 || !(args[0] is LinearGradientBrush))
				throw new Exception("End expects 3 parameters: LinearGradientBrush brush, double x, double y");
			var brush = (LinearGradientBrush)args[0];
			brush.EndPoint = new Point(Convert.ToDouble(args[1]), Convert.ToDouble(args[2]));
			return brush;
		}

		private static object RadialGradient(object[] args)
		{
			if (args.Length > 0)
				throw new Exception("RadialGradient expects no parameters");
			var radialGradient = new RadialGradientBrush();
			return radialGradient;
		}

		private static object Origin(object[] args)
		{
			if (args.Length != 3 || !(args[0] is RadialGradientBrush))
				throw new Exception("Origin expects 3 parameters: RadialGradientBrush brush, double x, double y");
			var brush = (RadialGradientBrush)args[0];
			brush.GradientOrigin = new Point(Convert.ToDouble(args[1]), Convert.ToDouble(args[2]));
			return brush;
		}

		private static object Center(object[] args)
		{
			if (args.Length != 3 || !(args[0] is RadialGradientBrush))
				throw new Exception("Center expects 3 parameters: RadialGradientBrush brush, double x, double y");
			var brush = (RadialGradientBrush)args[0];
			brush.Center = new Point(Convert.ToDouble(args[1]), Convert.ToDouble(args[2]));
			return brush;
		}

		private static object Radius(object[] args)
		{
			if (args.Length != 3 || !(args[0] is RadialGradientBrush))
				throw new Exception("Radius expects 3 parameters: RadialGradientBrush brush, double x, double y");
			var brush = (RadialGradientBrush)args[0];
			brush.RadiusX = Convert.ToDouble(args[1]);
			brush.RadiusY = Convert.ToDouble(args[2]);
			return brush;
		}

		private static object GradientStops(object[] args)
		{
			if (args.Length == 0 || !(args[0] is GradientBrush))
				throw new Exception("GradientStops expects parameters: [string color1, double offset1], [string color2, double offset2], ...");
			var arrays = new List<object[]>();
			for(int i = 1; i < args.Length; i++)
			{
				var array = args[i] as IEnumerable<object>;
				if(array == null || array.Count() != 2)
					throw new Exception("GradientStops expects parameters: [string color1, double offset1], [string color2, double offset2], ...");
				arrays.Add(array.ToArray());
			}
			var brush = (GradientBrush)args[0];
			foreach (var array in arrays)
				brush.GradientStops.Add(new GradientStop((Color)Color(new object[] { array[0] }), Convert.ToDouble(array[1])));
			return brush;
		}
	}
}
