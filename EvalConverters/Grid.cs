﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EvalConverters
{
	public class Grid : DependencyObject
	{
		public static readonly DependencyProperty RowHeightsProperty = DependencyProperty.RegisterAttached("RowHeights", typeof(string), typeof(Grid), new PropertyMetadata(null, new PropertyChangedCallback(OnRowHeightsChanged)));
		private static void OnRowHeightsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SetRowHeights(d, e.NewValue.ToString());
		}
		public static void SetRowHeights(DependencyObject depObj, string value)
		{
			if(depObj is System.Windows.Controls.Grid)
			{
				var grid = (System.Windows.Controls.Grid)depObj;
				grid.RowDefinitions.Clear();
				var result = ((IEnumerable<object>)Setup.Runner.Run(value, s => null)).Select(x => (GridLength)x);
				foreach (var gridLength in result)
					grid.RowDefinitions.Add(new RowDefinition() { Height = gridLength });
			}
		}


		public static readonly DependencyProperty ColumnWidthsProperty = DependencyProperty.RegisterAttached("ColumnWidths", typeof(string), typeof(Grid), new PropertyMetadata(null, new PropertyChangedCallback(OnColumnWidthsChanged)));
		private static void OnColumnWidthsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SetColumnWidths(d, e.NewValue.ToString());
		}
		public static void SetColumnWidths(DependencyObject depObj, string value)
		{
			if(depObj is System.Windows.Controls.Grid)
			{
				var grid = (System.Windows.Controls.Grid)depObj;
				grid.ColumnDefinitions.Clear();
				var result = ((IEnumerable<object>)Setup.Runner.Run(value, s => null)).Select(x => (GridLength)x);
				foreach (var gridLength in result)
					grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = gridLength });
			}
		}


		public static readonly DependencyProperty CellProperty = DependencyProperty.RegisterAttached("Cell", typeof(string), typeof(UIElement), new PropertyMetadata(null, new PropertyChangedCallback(OnCellChanged)));
		private static void OnCellChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SetCell(d, e.NewValue.ToString());
		}
		public static void SetCell(DependencyObject depObj, string value)
		{
			if(depObj is UIElement)
			{
				var uie = (UIElement)depObj;
				var ints = value.Split(' ', ',')
					.Where(x => !string.IsNullOrWhiteSpace(x))
					.Select(x => Convert.ToInt32(x))
					.ToList();
				if (ints.Count != 2)
					throw new ArgumentException("Cell value should be of the format \"rowInt,columnInt\" or \"rowInt columnInt\"");
				System.Windows.Controls.Grid.SetRow(uie, ints[0]);
				System.Windows.Controls.Grid.SetColumn(uie, ints[1]);
			}
		}
	}
}
