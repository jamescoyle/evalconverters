﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace EvalConverters
{
	[ContentProperty("ItemBindings")]
	public class CollectionBinding : FrameworkElement
	{
		public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(CollectionBinding), new PropertyMetadata(OnItemsSourceChanged));
		public IEnumerable ItemsSource
		{
			get => (IEnumerable)GetValue(ItemsSourceProperty);
			set => SetValue(ItemsSourceProperty, value);
		}
		private static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var collectionBinding = d as CollectionBinding;
			
			if(e.NewValue != null)
			{
				collectionBinding.UpdateMultiBinding();
				if (e.NewValue is INotifyCollectionChanged)
					(e.NewValue as INotifyCollectionChanged).CollectionChanged += collectionBinding.OnItemsSourceItemsChanged;
			}
		}
		private void OnItemsSourceItemsChanged(object sender, EventArgs e)
		{
			UpdateMultiBinding();
		}

		public ObservableCollection<ItemBinding> ItemBindings { get; private set; } = new ObservableCollection<ItemBinding>();

		public static DependencyProperty ResultProperty = DependencyProperty.Register("Result", typeof(object), typeof(CollectionBinding));
		public object Result => GetValue(ResultProperty);
		
		public IMultiValueConverter Converter { get; set; }

		public CultureInfo ConverterCulture { get; set; }

		public object ConverterParameter { get; set; }


		public CollectionBinding()
		{
			ItemBindings.CollectionChanged += (s, e) => UpdateMultiBinding();
		}

		private void UpdateMultiBinding()
		{
			if (ItemsSource == null || ItemBindings.Count == 0)
				return;

			BindingOperations.ClearBinding(this, ResultProperty);

			var multiBinding = new MultiBinding();
			multiBinding.Mode = BindingMode.OneWay;
			multiBinding.Converter = Converter;
			multiBinding.ConverterCulture = ConverterCulture;
			multiBinding.ConverterParameter = ConverterParameter;
			foreach(var item in ItemsSource)
			{
				foreach(var bindingDef in ItemBindings)
				{
					var newBinding = CloneBinding(bindingDef);
					newBinding.Source = item;
					multiBinding.Bindings.Add(newBinding);
				}
			}
			BindingOperations.SetBinding(this, ResultProperty, multiBinding);
		}

		private Binding CloneBinding(ItemBinding binding)
		{
			var newBinding = new Binding();
			if (newBinding.AsyncState != binding.AsyncState)
				newBinding.AsyncState = binding.AsyncState;
			if (newBinding.BindingGroupName != binding.BindingGroupName)
				newBinding.BindingGroupName = binding.BindingGroupName;
			if (newBinding.BindsDirectlyToSource != binding.BindsDirectlyToSource)
				newBinding.BindsDirectlyToSource = binding.BindsDirectlyToSource;
			if (newBinding.Converter != binding.Converter)
				newBinding.Converter = binding.Converter;
			if (newBinding.ConverterCulture != binding.ConverterCulture)
				newBinding.ConverterCulture = binding.ConverterCulture;
			if (newBinding.ConverterParameter != binding.ConverterParameter)
				newBinding.ConverterParameter = binding.ConverterParameter;
			if (newBinding.Delay != binding.Delay)
				newBinding.Delay = binding.Delay;
			if (newBinding.ElementName != binding.ElementName)
				newBinding.ElementName = binding.ElementName;
			if (newBinding.FallbackValue != binding.FallbackValue)
				newBinding.FallbackValue = binding.FallbackValue;
			if (newBinding.IsAsync != binding.IsAsync)
				newBinding.IsAsync = binding.IsAsync;
			if (newBinding.Mode != binding.Mode)
				newBinding.Mode = binding.Mode;
			if (newBinding.NotifyOnSourceUpdated != binding.NotifyOnSourceUpdated)
				newBinding.NotifyOnSourceUpdated = binding.NotifyOnSourceUpdated;
			if (newBinding.NotifyOnTargetUpdated != binding.NotifyOnTargetUpdated)
				newBinding.NotifyOnTargetUpdated = binding.NotifyOnTargetUpdated;
			if (newBinding.NotifyOnValidationError != binding.NotifyOnValidationError)
				newBinding.NotifyOnValidationError = binding.NotifyOnValidationError;
			if (newBinding.Path != binding.Path)
				newBinding.Path = binding.Path;
			if (newBinding.RelativeSource != binding.RelativeSource)
				newBinding.RelativeSource = binding.RelativeSource;
			if (newBinding.Source != binding.Source)
				newBinding.Source = binding.Source;
			if (newBinding.StringFormat != binding.StringFormat)
				newBinding.StringFormat = binding.StringFormat;
			if (newBinding.TargetNullValue != binding.TargetNullValue)
				newBinding.TargetNullValue = binding.TargetNullValue;
			if (newBinding.UpdateSourceExceptionFilter != binding.UpdateSourceExceptionFilter)
				newBinding.UpdateSourceExceptionFilter = binding.UpdateSourceExceptionFilter;
			if (newBinding.UpdateSourceTrigger != binding.UpdateSourceTrigger)
				newBinding.UpdateSourceTrigger = binding.UpdateSourceTrigger;
			if (newBinding.ValidatesOnDataErrors != binding.ValidatesOnDataErrors)
				newBinding.ValidatesOnDataErrors = binding.ValidatesOnDataErrors;
			if (newBinding.ValidatesOnExceptions != binding.ValidatesOnExceptions)
				newBinding.ValidatesOnExceptions = binding.ValidatesOnExceptions;
			if (newBinding.ValidatesOnNotifyDataErrors != binding.ValidatesOnNotifyDataErrors)
				newBinding.ValidatesOnNotifyDataErrors = binding.ValidatesOnNotifyDataErrors;
			foreach (var validationRule in binding.ValidationRules)
				newBinding.ValidationRules.Add(validationRule);
			if (newBinding.XPath != binding.XPath)
				newBinding.XPath = binding.XPath;
			return newBinding;
		}
	}


	public class ItemBinding
	{
		public object AsyncState { get; set; }
		public string BindingGroupName { get; set; }
		public bool BindsDirectlyToSource { get; set; }
		public IValueConverter Converter { get; set; }
		public CultureInfo ConverterCulture { get; set; }
		public object ConverterParameter { get; set; }
		public int Delay { get; set; }
		public string ElementName { get; set; }
		public object FallbackValue { get; set; }
		public bool IsAsync { get; set; }
		public BindingMode Mode { get; set; }
		public bool NotifyOnSourceUpdated { get; set; }
		public bool NotifyOnTargetUpdated { get; set; }
		public bool NotifyOnValidationError { get; set; }
		public PropertyPath Path { get; set; }
		public RelativeSource RelativeSource { get; set; }
		public object Source { get; set; }
		public string StringFormat { get; set; }
		public object TargetNullValue { get; set; }
		public UpdateSourceExceptionFilterCallback UpdateSourceExceptionFilter { get; set; }
		public UpdateSourceTrigger UpdateSourceTrigger { get; set; }
		public bool ValidatesOnDataErrors { get; set; }
		public bool ValidatesOnExceptions { get; set; }
		public bool ValidatesOnNotifyDataErrors { get; set; }
		public List<ValidationRule> ValidationRules { get; set; } = new List<ValidationRule>();
		public string XPath { get; set; }
	}
}
