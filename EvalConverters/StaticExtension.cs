﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace EvalConverters
{
	public class StaticExtension : MarkupExtension
	{
		public string Get { get; set; }

		public StaticExtension()
		{
		}

		public StaticExtension(string get)
		{
			Get = get;
		}

		public override object ProvideValue(IServiceProvider provider)
		{
			if (Get == null)
				throw new NotImplementedException();
			return Setup.Runner.Run(Get, s => null);
		}
	}
}
