﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConvertersUITest
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public ObservableCollection<OrderLine> Lines { get; private set; } = new ObservableCollection<OrderLine>();

		private Random _random = new Random();

		public MainWindow()
		{
			DataContext = this;
			InitializeComponent();
		}
		
		private void AddBtn_Click(object sender, RoutedEventArgs e)
		{
			Lines.Add(ModifyOrderLine(new OrderLine()));
		}

		private void EditBtn_Click(object sender, RoutedEventArgs e)
		{
			ModifyOrderLine(Lines[_random.Next(Lines.Count)]);
		}

		private OrderLine ModifyOrderLine(OrderLine orderLine)
		{
			orderLine.Quantity = _random.Next(1, 10);
			orderLine.UnitPrice = _random.NextDouble() * 10;
			return orderLine;
		}
	}

	public class OrderLine : INotifyPropertyChanged
	{
		private double _unitPrice;
		public double UnitPrice
		{
			get => _unitPrice;
			set
			{
				_unitPrice = value;
				DispatchPropertyChanged(nameof(UnitPrice));
			}
		}

		private int _quantity;
		public int Quantity
		{
			get => _quantity;
			set
			{
				_quantity = value;
				DispatchPropertyChanged(nameof(Quantity));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected void DispatchPropertyChanged(string propertyName) =>
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}
}
