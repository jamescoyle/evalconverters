﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConvertersUITest
{
    /// <summary>
    /// Interaction logic for PianoRollControl.xaml
    /// </summary>
    public partial class PianoRollControl : UserControl
    {
        public PianoRollControl()
        {
			DataContext = new PianoRollControlViewModel();
            InitializeComponent();
        }
    }


	public class PianoRollControlViewModel : INotifyPropertyChanged
	{
		private int _pitchHeight = 10;
		public int PitchHeight
		{
			get => _pitchHeight;
			set => Set(nameof(PitchHeight), ref _pitchHeight, value);
		}

		private int _beatCount = 16;
		public int BeatCount
		{
			get => _beatCount;
			set => Set(nameof(BeatCount), ref _beatCount, value);
		}

		private int _beatsPerBar = 4;
		public int BeatsPerBar
		{
			get => _beatsPerBar;
			set => Set(nameof(BeatsPerBar), ref _beatsPerBar, value);
		}

		private int _beatWidth = 30;
		public int BeatWidth
		{
			get => _beatWidth;
			set => Set(nameof(BeatWidth), ref _beatWidth, value);
		}

		private Brush _lightPitchBrush;
		public Brush LightPitchBrush
		{
			get => _lightPitchBrush;
			set => Set(nameof(LightPitchBrush), ref _lightPitchBrush, value);
		}

		private Brush _darkPitchBrush;
		public Brush DarkPitchBrush
		{
			get => _darkPitchBrush;
			set => Set(nameof(DarkPitchBrush), ref _darkPitchBrush, value);
		}

		private Brush _barLineBrush;
		public Brush BarLineBrush
		{
			get => _barLineBrush;
			set => Set(nameof(BarLineBrush), ref _barLineBrush, value);
		}

		private Brush _beatLineBrush;
		public Brush BeatLineBrush
		{
			get => _beatLineBrush;
			set => Set(nameof(BeatLineBrush), ref _beatLineBrush, value);
		}

		private bool _darkMode = false;
		public bool DarkMode
		{
			get => _darkMode;
			set
			{
				if (Set(nameof(DarkMode), ref _darkMode, value))
					SetBrushes();
			}
		}

		public PianoRollControlViewModel()
		{
			SetBrushes();
		}

		private void SetBrushes()
		{
			if (DarkMode)
			{
				LightPitchBrush = new SolidColorBrush(Colors.Gray);
				DarkPitchBrush = new SolidColorBrush(Colors.DimGray);
				BarLineBrush = new SolidColorBrush(new Color() { A = 255, R = 50, G = 50, B = 50 });
				BeatLineBrush = new SolidColorBrush(Colors.LightSlateGray);
			}
			else
			{
				LightPitchBrush = new SolidColorBrush(Colors.LightGray);
				DarkPitchBrush = new SolidColorBrush(Colors.Gray);
				BarLineBrush = new SolidColorBrush(new Color() { A = 255, R = 50, G = 50, B = 50 });
				BeatLineBrush = new SolidColorBrush(Colors.DimGray);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void DispatchPropertyChanged(string propertyName) =>
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		private bool Set<T>(string name, ref T field, T value)
		{
			if (Equals(field, value))
				return false;
			field = value;
			DispatchPropertyChanged(name);
			return true;
		}
	}
}
